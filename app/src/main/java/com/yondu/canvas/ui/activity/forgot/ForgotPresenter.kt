package com.yondu.canvas.ui.activity.forgot

import android.annotation.SuppressLint
import com.yondu.canvas.global.mvp.MvpPresenter
import com.yondu.canvas.model.remote.body.UserBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketTimeoutException

class ForgotPresenter(view: ForgotView) : MvpPresenter<ForgotView>(view) {

    @SuppressLint("CheckResult")
    fun sendForgot(email:CharSequence){
        // send Forgot Api
        var forgotBody = UserBody()
        forgotBody.username = email.toString()

        api.postForgot(forgotBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { error ->
                if (error is SocketTimeoutException) {
                    view.onForgotFailed("Socket Timeout")
                } else {
                    val httpError = error as HttpException
                    view.onForgotFailed(httpError.body().message!!)
                }
            }
            .subscribe({ response ->
                view.onForgotSuccess(response.message!!)
                },{})
    }
}