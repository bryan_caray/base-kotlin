package com.yondu.canvas.ui.activity.splash

import android.content.Intent
import com.yondu.canvas.databinding.ActivitySplashBinding
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.ui.activity.home.HomeActivity
import com.yondu.canvas.ui.activity.landing.LandingActivity
import android.os.Handler
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import kotlinx.android.synthetic.main.activity_splash.*
import com.google.android.gms.common.util.IOUtils.toByteArray
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.util.Base64
import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashActivity : MvpActivity<ActivitySplashBinding, SplashPresenter, SplashView>(), SplashView {

    override val presenter get() = SplashPresenter(this)
    val isSharedElementTransitionEnabled = "IS_SHARED_ELEMENT_TRANSITION_ENABLED"
    override val layoutId get() = com.yondu.canvas.R.layout.activity_splash

    override fun onViewCreated() {
        try {
            val info = packageManager.getPackageInfo(
                "com.yondu.canvas",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }


        val handler = Handler()
        handler.postDelayed(Runnable {
            presenter.checkIsLoggedIn()
        }, 2000)
    }
    override fun startHome(){
        val p1 = Pair.create(imgLogo as View, "image_transition")
        var intent = Intent(this, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(isSharedElementTransitionEnabled, true)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1)
        startActivity(intent,options.toBundle())
    }
    override fun startLanding(){
        val p1 = Pair.create(imgLogo as View, "image_transition")
        var intent = Intent(this, LandingActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1)
        intent.putExtra("IS_SHARED_ELEMENT_TRANSITION_ENABLED", true)
        startActivity(intent,options.toBundle())
    }
}
