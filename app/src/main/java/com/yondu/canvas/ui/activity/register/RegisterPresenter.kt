package com.yondu.canvas.ui.activity.register

import android.annotation.SuppressLint
import android.text.Editable
import com.yondu.canvas.global.mvp.MvpPresenter
import com.yondu.canvas.model.remote.body.UserBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketTimeoutException

class RegisterPresenter(view: RegisterView) : MvpPresenter<RegisterView>(view) {

    @SuppressLint("CheckResult")
    fun submitRegistration(
        email: CharSequence,
        password: CharSequence,
        name: CharSequence,
        phoneNumber: CharSequence
    ) {
        var registerBody = UserBody()
        registerBody.confirmPassword = password.toString()
        registerBody.password = password.toString()
        registerBody.username = email.toString()

        //Process Registration Here
        api.postRawRegister(registerBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { error ->
                if (error is SocketTimeoutException) {
                    view.onFailedRegister("Socket Timeout")
                } else {
                    val httpError = error as HttpException
                    view.onFailedRegister(httpError.body().message!!)
                }
            }
            .subscribe({ response ->
                view.onSuccessRegister(response!!.message!!)
            }, {
            })
    }

    @SuppressLint("CheckResult")
    fun submitWhitelistRegistration(
        email: CharSequence,
        name: Editable,
        phoneNumber: CharSequence
    ) {
        var registerBody = UserBody()
        registerBody.username = email.toString()

        //Process Registration Here
        api.postRawWhitelistRegister(registerBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { error ->
                if (error is SocketTimeoutException) {
                    view.onFailedRegister("Socket Timeout")
                } else {
                    val httpError = error as HttpException
                    view.onFailedRegister(httpError.body().message!!)
                }
            }
            .subscribe({ response ->
                view.onSuccessRegister(response!!.message!!)
            }, {
            })
    }


}