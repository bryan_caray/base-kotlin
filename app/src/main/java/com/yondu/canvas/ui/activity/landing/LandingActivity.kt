package com.yondu.canvas.ui.activity.landing

import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityLandingBinding
import com.yondu.canvas.global.api.FacebookClient
import com.yondu.canvas.global.api.FacebookClient.Companion.mFacebookSignInClient
import com.yondu.canvas.global.api.GoogleClient
import com.yondu.canvas.global.api.GoogleClient.Companion.mGoogleSignInClient
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.ui.activity.login_owasp.LoginOwaspActivity
import com.yondu.canvas.ui.activity.main.MainActivity
import com.yondu.canvas.ui.activity.register.RegisterActivity
import com.yondu.canvas.ui.activity.register.WhitelistRegisterActivity
import kotlinx.android.synthetic.main.activity_splash.*


class LandingActivity : MvpActivity<ActivityLandingBinding, LandingPresenter, LandingView>(),
    LandingView {
    override fun failedGoogleLogin(e: String) {
        Toast.makeText(this, e, Toast.LENGTH_SHORT).show()
    }

    override fun successGoogleLogin(account: GoogleSignInAccount?) {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
    }

    override val presenter get() = LandingPresenter(this)

    override val layoutId get() = R.layout.activity_landing
    private var doubleBackToExitPressedOnce = false
    private val RC_SIGN_IN_GOOGLE: Int = 65535

    override fun onViewCreated() {
//        if(PreferenceUtil.instance.hasUser){
//            val intent = Intent(this, HomeActivity::class.java)
//            startActivity(intent)
//        }
//        else{

        supportPostponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgLogo.transitionName = "image_transition"
        }
        Glide.with(this)
            .load(getDrawable(R.drawable.logo))
            .dontAnimate()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }
            })
            .into(imgLogo)


        binding.txtCreate.setOnClickListener {

            if (hasNetworkConnection()) {
                val selection = arrayOf("Whitelist", "Normal", "Mobile")

                val builder = AlertDialog.Builder(this)
                builder.setTitle("Register via")
                var intent: Intent = Intent()
                builder.setItems(selection, DialogInterface.OnClickListener { dialog, which ->

                    when (which) {
                        0 -> intent = Intent(this, WhitelistRegisterActivity::class.java)

                        1 -> intent = Intent(this, RegisterActivity::class.java)

                    }
                    if (which < 2) {
                        dialog.dismiss()
                        startActivity(intent)
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
                    }
                    // the user clicked on colors[which]
                })
                builder.show()
            } else {
                noInternetConnection()
            }
        }
//        }

        GoogleClient.createInstance(this)
        FacebookClient.createInstance()
        binding.signinFB.setPermissions("email", "public_profile")
        // Callback registration
        binding.signinFB.registerCallback(mFacebookSignInClient, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                    try {
                        //here is the data that you want
                        Log.d("FBLOGIN_JSON_RES", `object`.toString())


                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }

                val parameters = Bundle()
                parameters.putString("fields", "name,email,id,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()

                Log.d(TAG, "facebook:onSuccess:$loginResult")
//                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                Log.d(TAG, "facebook:onCancel")
                // ...
            }

            override fun onError(error: FacebookException) {
                Log.d(TAG, "facebook:onError", error)
                // ...
            }
        })
        binding.signinGoogle.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode === RC_SIGN_IN_GOOGLE) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            presenter.handleSignInResult(task)
        }
        mFacebookSignInClient.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    fun login() {
        val p1 = Pair.create(imgLogo as View, "image_transition")
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1)
        if (hasNetworkConnection()) {
            val selection = arrayOf("Normal", "OWASP")
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Login via")
            var intent: Intent = Intent()
            builder.setItems(selection, DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    0 -> intent = Intent(this, MainActivity::class.java)

                    1 -> intent = Intent(this, LoginOwaspActivity::class.java)
                }
                dialog.dismiss()
                startActivity(intent, options.toBundle())

                // the user clicked on colors[which]
            })
            builder.show()
        } else {
            noInternetConnection()
        }

    }

}
