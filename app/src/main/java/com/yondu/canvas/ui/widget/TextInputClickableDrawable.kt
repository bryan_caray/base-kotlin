package com.yondu.canvas.ui.widget

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.util.AttributeSet
import android.view.MotionEvent
import com.google.android.material.textfield.TextInputEditText
import com.yondu.canvas.R
import com.yondu.canvas.global.util.ResourceUtil

open class TextInputClickableDrawable : TextInputEditText {

    private lateinit var listener: OnDrawableClickListener

    private var left: Drawable? = null

    private var top: Drawable? = null

    private var right: Drawable? = null

    private var bottom: Drawable? = null

    var fuzz = 10

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setCompoundDrawables(left: Drawable?, top: Drawable?, right: Drawable?, bottom: Drawable?) {
        this.left = left
        this.top = top
        this.right = right
        this.bottom = bottom
        return super.setCompoundDrawables(left, top, right, bottom)
    }

    override fun setCompoundDrawablesWithIntrinsicBounds(left: Drawable?, top: Drawable?, right: Drawable?, bottom: Drawable?) {
        this.left = left
        this.top = top
        this.right = right
        this.bottom = bottom
        return super.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x.toInt()
        val y = event.y.toInt()

        if (event.action == MotionEvent.ACTION_DOWN) {
            val bounds: Rect

            when {
                left != null -> {
                    bounds = left!!.bounds
                    if (x >= (paddingLeft - fuzz))
                        if (x <= (paddingLeft + bounds.width() + fuzz))
                            if (y >= (paddingTop - fuzz))
                                if (y <= (height - paddingBottom + fuzz))
                                    listener.onLeftDrawableClick()
                }

                top != null -> {
                    bounds = top!!.bounds
                    if (x >= (paddingLeft - fuzz))
                        if (x <= (width - paddingRight + fuzz))
                            if (y >= (paddingTop - fuzz))
                                if (y <= (paddingTop + bounds.height() + fuzz))
                                    listener.onTopDrawableClick()
                }

                right != null -> {
                    bounds = right!!.bounds
                    if (x >= (width - paddingRight - bounds.width() - fuzz))
                        if (x <= (width - paddingRight + fuzz))
                            if (y >= (paddingTop - fuzz))
                                if (y <= (height - paddingBottom + fuzz))
                                    listener.onRightDrawableClick()
                }

                bottom != null -> {
                    bounds = bottom!!.bounds
                    if (x >= (paddingLeft - fuzz))
                        if (x <= (width - paddingRight + fuzz))
                            if (y >= (height - paddingBottom - bounds.height() - fuzz))
                                if (y <= (height - paddingBottom + fuzz))
                                    listener.onBottomDrawableClick()
                }
            }
        }

        return super.onTouchEvent(event)
    }

    fun setDrawableClickListener(listener: OnDrawableClickListener) {
        this.listener = listener
    }

    interface OnDrawableClickListener {
        fun onLeftDrawableClick() {}
        fun onTopDrawableClick() {}
        fun onRightDrawableClick() {}
        fun onBottomDrawableClick() {}
    }
}