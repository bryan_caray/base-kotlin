package com.yondu.canvas.ui.activity.home

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityHomeBinding
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.global.mvp.MvpFragment
import com.yondu.canvas.global.util.PreferenceUtil
import com.yondu.canvas.ui.activity.changepassword.ChangePasswordActivity
import com.yondu.canvas.ui.activity.home.fragment.FragmentDashboard
import com.yondu.canvas.ui.activity.home.fragment.FragmentHome
import com.yondu.canvas.ui.activity.home.fragment.FragmentNotification
import com.yondu.canvas.ui.activity.landing.LandingActivity
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.activity_splash.*


class HomeActivity : MvpActivity<ActivityHomeBinding, HomePresenter, HomeView>(),
    HomeView, BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val backStack = "$item"
        when (item.itemId) {
            R.id.navigation_home -> {
                openFragment(MvpFragment.newInstance(FragmentHome::class.java), backStack)
            }
            R.id.navigation_dashboard -> {
                openFragment(MvpFragment.newInstance(FragmentDashboard::class.java), backStack)

//                openFragment(MvpFragment.newInstance(FragmentDashboard::class.java), backStack)
            }
            R.id.navigation_notifications -> {
                openFragment(MvpFragment.newInstance(FragmentNotification::class.java), backStack)

//                openFragment(MvpFragment.newInstance(FragmentNotification::class.java), backStack)
            }
        }
        return true
    }

    override val fragmentViewId = R.id.content
    private var doubleBackToExitPressedOnce = false
    private val isSharedElementTransitionEnabled = "IS_SHARED_ELEMENT_TRANSITION_ENABLED"

    override fun showChangePassword() {
        val intent = Intent(this, ChangePasswordActivity::class.java)
        startActivity(intent)
    }


    override fun onSuccessFetch() {
        closeDialogBox()
    }

    override fun onFailedFetch(error: String) {
        super.onFailedFetch(error)
        closeDialogBox()
        openErrorBox(error)
    }

    override fun successLogout() {
        val intent = Intent(this, LandingActivity::class.java)
        startActivity(intent)
        finish()
    }

    override val presenter get() = HomePresenter(this)

    override val layoutId get() = R.layout.activity_home


    override fun onViewCreated() {
        openProgressBox()
        if (intent.extras != null) {
            if (intent.extras.getBoolean(isSharedElementTransitionEnabled)) {
                supportPostponeEnterTransition()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imgLogo.transitionName = "image_transition"
                }
                Glide.with(this)
                    .load(getDrawable(R.drawable.logo))
                    .dontAnimate()
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any,
                            target: Target<Drawable>,
                            isFirstResource: Boolean
                        ): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable,
                            model: Any,
                            target: Target<Drawable>,
                            dataSource: DataSource,
                            isFirstResource: Boolean
                        ): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }
                    })
                    .into(imgLogo)
            }
        }
        var header: View = binding.navigationView.getHeaderView(0)
        var textEmail = header.findViewById<TextView>(R.id.tv_email)
        var textName = header.findViewById<TextView>(R.id.tv_name)
        textEmail.text = PreferenceUtil.instance.email
        textName.text = PreferenceUtil.instance.name
        presenter.getPosts()

        binding.navigation.setOnNavigationItemSelectedListener(this)
        binding.navigation.selectedItemId = R.id.navigation_home


        binding.navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                com.yondu.canvas.R.id.nav_item_logout -> binding.drawerView.consume { presenter.logOut() }
                com.yondu.canvas.R.id.nav_item_about -> binding.drawerView.consume { presenter.changePassword() }
                else -> super.onOptionsItemSelected(it)
            }
        }
        binding.appBar.menu.setOnClickListener {
            binding.drawerView.openDrawer(GravityCompat.START)
        }
    }

    inline fun DrawerLayout.consume(f: () -> Unit): Boolean {
        f()
        closeDrawers()
        return true
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }


}
