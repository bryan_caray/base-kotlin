package com.yondu.canvas.ui.helper

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.yondu.canvas.R
import com.yondu.canvas.global.api.ApiClient
import com.yondu.canvas.global.util.ResourceUtil
import com.yondu.canvas.model.local.CountryDial
import com.yondu.canvas.ui.dialog.CountryDialsDialog
import com.yondu.canvas.ui.widget.TextInputClickableDrawable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.registeranyemail.view.*
import java.io.InputStreamReader
import java.util.*

class MobileNumber(private val activity: Activity) : CountryDialsDialog.ItemListener {

    private var tiMobile = activity.include.txtRegPhone
    private lateinit var countDialog : AlertDialog
    init {
        val countryDials = ApiClient.instance.gson
            .fromJson(InputStreamReader(activity.resources
                .openRawResource(R.raw.country_dials)), Array<CountryDial>::class.java)
        val default = countryDials.first { it.code!! == Locale.getDefault().country }
        countDialog = CountryDialsDialog(activity, countryDials.sortedBy { it.name }, this@MobileNumber).initView().create()

        tiMobile.setPrefix(ResourceUtil.getFlagDrawable(activity, default.code!!), "${default.prefix!!} ")
        tiMobile.setDrawableClickListener(object : TextInputClickableDrawable.OnDrawableClickListener {
            override fun onLeftDrawableClick() {
               if(countDialog == null) {
                   countDialog = CountryDialsDialog(activity, countryDials.sortedBy { it.name }, this@MobileNumber).initView().create()
               }
                countDialog.show()
            }
        })
    }

    override fun onItemSelected(item: CountryDial) {
     if(countDialog != null) {
         tiMobile.setPrefix(ResourceUtil.getFlagDrawable(activity, item.code!!), "${item.prefix} ")
         countDialog.dismiss()
     }

    }
}
