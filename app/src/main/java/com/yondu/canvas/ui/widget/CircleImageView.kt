package com.yondu.canvas.ui.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class CircleImageView : AppCompatImageView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        val drawable = drawable as BitmapDrawable
        val b = drawable.bitmap
        val bitmap = b.copy(Bitmap.Config.ARGB_8888, true)
        val round = bitmap.crop(Math.min(width, height))
        canvas.drawBitmap(round, 0f, 0f, null)
    }

    private fun Bitmap.crop(radius: Int): Bitmap {
        val finalBitmap = Bitmap.createScaledBitmap(this, radius, radius, false)

        val output = Bitmap.createBitmap(finalBitmap.width, finalBitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)
        val rect = Rect(0, 0, finalBitmap.width, finalBitmap.height)
        val paint = Paint()
        val w = finalBitmap.width / 2
        val h = finalBitmap.width / 2

        paint.isAntiAlias = true
        paint.isFilterBitmap = true
        paint.isDither = true

        canvas.drawARGB(0, 0, 0, 0)
        canvas.drawCircle(w.toFloat(), h.toFloat(), w.toFloat(), paint)

        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(finalBitmap, rect, rect, paint)

        return output
    }
}