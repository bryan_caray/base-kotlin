package com.yondu.canvas.ui.activity.changepassword


import android.content.Intent
import android.view.View
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityChangepassBinding
import com.yondu.canvas.global.module.changepassword.ChangePassword
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.global.util.PreferenceUtil
import com.yondu.canvas.ui.activity.main.MainActivity
import com.yondu.canvas.ui.activity.walkthrough.WalkthroughActivity

class ChangePasswordActivity: MvpActivity<ActivityChangepassBinding, ChangePasswordPresenter, ChangePasswordView>(),
    ChangePasswordView,ChangePassword.Handler {
    override fun onCancel() {
        finish()
    }

    private var isSuccess = false

    override fun onPositiveClicked() {
        closeDialogBox()
        if(isSuccess){
            if(intent.extras != null) {
                if (intent.extras.getBoolean("isFirst")) {
                    var intent = Intent(this, WalkthroughActivity::class.java)
                    if (PreferenceUtil.instance.hasWalkthrough) {
                        intent = Intent(this, MainActivity::class.java)
                    }
                    startActivity(intent)
                }
            }
            finish()
        }
    }

    override fun onSuccessChange(message:String) {
        isSuccess = true
        closeDialogBox()
        //MainActivity is Login
        openMessageBox(message,"Successful Update Password",null,"Ok",true)
    }

    override fun onFailedChange(error: String) {
        closeDialogBox()
        openErrorBox(error,"Failed Change Password",null,"OK",true)  }

    override fun onSubmit(tempPassword: CharSequence,password: CharSequence) {
        openProgressBox()
        presenter.submitChange(tempPassword,password)
    }

    override val presenter: ChangePasswordPresenter
        get() = ChangePasswordPresenter(this)

    override val layoutId get() = R.layout.activity_changepass

    override fun onViewCreated() {
        if(intent.extras != null) {
            if (intent.extras.getBoolean("isFirst")){
                binding.include.lblChngCnl.visibility = View.GONE
            }
        }
       ChangePassword(binding.include,this)

        }
}








