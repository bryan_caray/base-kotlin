package com.yondu.canvas.ui.helper

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.ViewPager
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.yondu.canvas.databinding.ActivityMainBinding
import com.yondu.canvas.global.mvp.MvpPagerAdapter
import com.yondu.canvas.global.util.TextUtil
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.login_email_password.view.*

class EmailPassword(binding: ActivityMainBinding, handler: Handler) {

    private val pager = binding.include.pLogin

    init {
        pager.adapter = object : MvpPagerAdapter<ConstraintLayout>(listOf(pager.include.email, pager.include.password)) {

        }

        pager.bProceed.setOnClickListener {
            if (validateEmail(pager.tiEmail, pager.ilEmail))
                handler.onProceed(pager, pager.tiEmail.text!!)
        }

        pager.bSubmit.setOnClickListener {
            if (validatePassword(pager.tiPassword, pager.ilPassword))
                handler.onSubmit(pager.tiEmail.text!!, pager.tiPassword.text!!)
        }

        pager.bBack.setOnClickListener {
            pager.tiPassword.text = null
            pager.setCurrentItem(0, true)
        }


        pager.lblLoginCancel.setOnClickListener {
            handler.onCancelLogin()
        }

        pager.bForgot.setOnClickListener {
            handler.onForgotLogin(pager.tiEmail.text.toString())
        }
    }

    private fun validateEmail(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val email = input.text!!

        when {
            email.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }

            !TextUtil.isEmail(email) -> {
                TextUtil.setError(input, layout, "${input.hint} is invalid")
                return false
            }
        }

        return true
    }

    private fun validatePassword(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val password = input.text!!

        when {
            password.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }
        }

        return true
    }

    interface Handler {
        fun onProceed(pager: ViewPager, email: CharSequence) {
            pager.setCurrentItem(1, true)
        }
        fun onCancelLogin()
        fun onSubmit(email: CharSequence, password: CharSequence)
        fun onForgotLogin(email: CharSequence)
    }
}