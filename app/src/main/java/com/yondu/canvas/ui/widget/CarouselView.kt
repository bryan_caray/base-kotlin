package com.yondu.canvas.ui.widget

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.Interpolator
import android.widget.Scroller
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.yondu.canvas.R

class CarouselView : ViewPager {

    private lateinit var adapter: CarouselAdapter

    private var scroller: CarouselScroller

    private val scrollMessage = 0

    private var reversed = false

    private var interval = 0

    private var scrollFactor = 0f

    private var stopWhenTouched = true

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.CarouselView, 0, 0)
        interval = attributes.getInteger(R.styleable.CarouselView_interval, 1500)
        scrollFactor = attributes.getFloat(R.styleable.CarouselView_scrollFactor, 2.5f)
        stopWhenTouched = attributes.getBoolean(R.styleable.CarouselView_stopWhenTouched, true)
    }

    init {
        val mScroller = ViewPager::class.java.getDeclaredField("mScroller")
        mScroller.isAccessible = true

        val sInterpolator = ViewPager::class.java.getDeclaredField("sInterpolator")
        sInterpolator.isAccessible = true

        scroller = CarouselScroller(context, sInterpolator.get(null) as Interpolator)
        mScroller.set(this, scroller)
    }

    fun setAdapter(adapter: CarouselAdapter) {
        super.setAdapter(adapter)
        this.adapter = adapter
        scroller.factor = scrollFactor
        startAutoScroll(interval)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (stopWhenTouched) {
            if (event.action == MotionEvent.ACTION_DOWN)
                stopAutoScroll()

            if (event.action == MotionEvent.ACTION_UP ||
                event.action == MotionEvent.ACTION_CANCEL)
                startAutoScroll(interval)
        }

        return super.onTouchEvent(event)
    }

    private fun startAutoScroll(interval: Int) {
        sendScrollMessage(interval)
    }

    private fun stopAutoScroll() {
        scrollHandler.removeMessages(scrollMessage)
    }

    private fun sendScrollMessage(interval: Int) {
        scrollHandler.removeMessages(scrollMessage)
        scrollHandler.sendEmptyMessageDelayed(scrollMessage,
            if (currentItem == 0 || currentItem == adapter.count - 1) (interval / 2).toLong() else interval.toLong())
    }

    private fun doScroll() {
        val total = adapter.count
        if (total > 1) {
            val next = if (reversed) currentItem - 1 else currentItem + 1

            when (next) {
                -1 -> reversed = false
                total -> reversed = true
            }

            setCurrentItem(next, true)
        }
    }

    override fun setCurrentItem(item: Int, smoothScroll: Boolean) {
        stopAutoScroll()
        super.setCurrentItem(item, smoothScroll)
        startAutoScroll(interval)
    }

    @SuppressLint("HandlerLeak")
    private val scrollHandler = object : Handler() {

        override fun handleMessage(message: Message) {
            super.handleMessage(message)

            when (message.what) {
                scrollMessage -> {
                    doScroll()
                    sendScrollMessage(interval)
                }
            }
        }
    }

    private inner class CarouselScroller(context: Context, interpolator: Interpolator) :
        Scroller(context, interpolator) {

        var factor = 0f

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) =
            super.startScroll(startX, startY, dx, dy, (duration * factor).toInt())
    }

    abstract class CarouselAdapter(protected var items: List<Any>) : PagerAdapter() {

        override fun getCount(): Int = items.size

        override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

        override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
            container.removeView(obj as View)
        }
    }
}