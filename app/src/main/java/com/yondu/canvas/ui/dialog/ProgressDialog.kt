package com.yondu.canvas.ui.dialog

import android.app.Activity
import com.yondu.canvas.R
import com.yondu.canvas.databinding.DialogProgressBinding
import com.yondu.canvas.global.mvp.MvpDialog

class ProgressDialog(activity: Activity) :
    MvpDialog<DialogProgressBinding>(activity) {

    override var layoutId = R.layout.dialog_progress

    fun setMessage(message: CharSequence) {
        binding.tvDescription.text = message
    }

    override fun onViewCreated() {

    }
}