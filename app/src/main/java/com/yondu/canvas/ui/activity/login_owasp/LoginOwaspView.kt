package com.yondu.canvas.ui.activity.login_owasp

import com.yondu.canvas.global.mvp.MvpView

interface LoginOwaspView : MvpView.ActivityFragment{
    fun onSuccessLogin(first: Boolean)
    fun onFailedLogin(message:String)

}