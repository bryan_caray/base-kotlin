package com.yondu.canvas.ui.activity.main

import android.annotation.SuppressLint
import com.yondu.canvas.global.mvp.MvpPresenter
import com.yondu.canvas.global.util.PreferenceUtil
import com.yondu.canvas.model.remote.body.UserBody
import com.yondu.canvas.model.remote.response.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.RealmChangeListener
import io.realm.RealmResults
import retrofit2.HttpException
import java.net.SocketTimeoutException


class MainPresenter(view: MainView) : MvpPresenter<MainView>(view) {
    @SuppressLint("CheckResult")
    fun submitLogin(email: CharSequence, password: CharSequence) {
        var loginBody = UserBody()
        loginBody.password = password.toString()
        loginBody.username = email.toString()
        realm.executeTransaction { realm ->
            val result = realm.where(User::class.java!!).findAll()
            result.deleteAllFromRealm()
        }
        //Process Registration Here
        var user: RealmResults<User> = realm.where(User::class.java).findAllAsync()

        user.addChangeListener(RealmChangeListener {
            realm.copyFromRealm(user)
        })

        api.postRawLogin(loginBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

            .doOnError { error ->
                if (error is SocketTimeoutException) {
                    view.onFailedLogin("Socket Timeout")

                } else {
                    val httpError = error as HttpException
                    view.onFailedLogin(httpError.body().message!!)
                }
            }
            .subscribe({ response ->
                realm.executeTransaction {
                    realm.insertOrUpdate(response.data!!.user)

                    PreferenceUtil.instance.name = response.data!!.user!!.name!!
                    PreferenceUtil.instance.email = response.data!!.user!!.email!!
                    PreferenceUtil.instance.hasUser = true
                    view.onSuccessLogin(response.data!!.first!!)
                }
            }, {

            })
    }
}