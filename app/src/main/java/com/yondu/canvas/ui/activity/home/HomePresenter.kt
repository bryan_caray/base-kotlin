package com.yondu.canvas.ui.activity.home

import android.annotation.SuppressLint
import android.util.Log
import com.yondu.canvas.global.mvp.MvpPresenter
import com.yondu.canvas.global.util.PreferenceUtil
import com.yondu.canvas.model.db.Post
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.RealmResults
import retrofit2.HttpException
import java.net.SocketTimeoutException

class HomePresenter(view: HomeView) : MvpPresenter<HomeView>(view) {
    fun logOut(){
        //api cal for logout
        PreferenceUtil.instance.hasUser = false
        view.successLogout()
    }

    fun changePassword(){
        view.showChangePassword()
    }

    @SuppressLint("CheckResult")
    fun getPosts() {
        api.posts("https://jsonplaceholder.typicode.com/posts")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { error ->
                if(error is SocketTimeoutException){
                    view.onFailedFetch("Socket Timeout")
                }else{
                    val httpError = error as HttpException
                    view.onFailedFetch(httpError.body().message!!)
                }
            }
            .subscribe({ response ->
                realm.beginTransaction()
                realm.insertOrUpdate(response)
                realm.commitTransaction()
                view.onSuccessFetch()
            }, {})
    }

    fun getRealmPost(): RealmResults<Post>{
        return realm.where(Post::class.java).findAll()
    }
}