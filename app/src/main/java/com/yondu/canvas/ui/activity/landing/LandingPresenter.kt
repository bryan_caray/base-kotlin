package com.yondu.canvas.ui.activity.landing
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.yondu.canvas.global.mvp.MvpPresenter
import com.google.android.gms.common.api.ApiException



class LandingPresenter(view: LandingView) : MvpPresenter<LandingView>(view) {
    fun handleSignInResult(completedTask: Task<GoogleSignInAccount>?) {
        try {
            val account = completedTask?.getResult(ApiException::class.java)
            // Signed in successfully, show authenticated UI.
            view.successGoogleLogin(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            view.failedGoogleLogin(e.toString()!!)
        }

    }
}