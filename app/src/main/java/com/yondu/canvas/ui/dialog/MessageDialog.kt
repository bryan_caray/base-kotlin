package com.yondu.canvas.ui.dialog

import android.app.Activity
import com.yondu.canvas.R
import com.yondu.canvas.databinding.DialogMessageBinding
import com.yondu.canvas.global.mvp.MvpDialog

class MessageDialog(activity: Activity, private val listener: OnClickListener) :
    MvpDialog<DialogMessageBinding>(activity) {

    override var layoutId = R.layout.dialog_message

    override fun onPositiveClicked() {
        super.onPositiveClicked()
        listener.onPositiveClicked()
    }

    override fun onNegativeClicked() {
        super.onNegativeClicked()
        listener.onNegativeClicked()
    }

    override fun onNeutralClicked() {
        super.onNeutralClicked()
        listener.onNeutralClicked()
    }

    fun setMessage(message: CharSequence) {
        binding.tvDescription.text = message
    }

    override fun onViewCreated() {

    }

    interface OnClickListener {
        fun onPositiveClicked() {}
        fun onNegativeClicked() {}
        fun onNeutralClicked() {}
    }
}