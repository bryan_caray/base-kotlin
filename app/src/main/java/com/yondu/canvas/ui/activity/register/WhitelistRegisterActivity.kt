package com.yondu.canvas.ui.activity.register

import android.content.Intent
import android.text.Editable
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityRegisterBinding
import com.yondu.canvas.global.module.register.RegisterWhitelist
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.ui.activity.main.MainActivity
import com.yondu.canvas.ui.helper.MobileNumberWhitelist

class WhitelistRegisterActivity : MvpActivity<ActivityRegisterBinding,RegisterPresenter, RegisterView>(),
    RegisterView, RegisterWhitelist.Handler{
    override fun onSubmit(email: CharSequence, name: Editable, phoneNumber: CharSequence) {
        openProgressBox()
        presenter.submitWhitelistRegistration(email,name,phoneNumber)
    }

    var isSuccess : Boolean = false


    override fun onFailedRegister(error: String) {
        closeDialogBox()
        openErrorBox(error,"Failed Register",null,"OK",true)

    }

    override fun onPositiveClicked() {
        closeDialogBox()
        if(isSuccess){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
            overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up)
        finish()
        }
    }

    override fun onSuccessRegister(message: String) {
        isSuccess = true
        //MainActivity is Login
        closeDialogBox()
        openMessageBox(message,"Successful Register",null,"Ok",true)
    }


    override val presenter get() = RegisterPresenter(this)

    override val layoutId get() = R.layout.activity_register



    override fun onViewCreated() {
        binding.isVisible = false
        binding.isWhitelistVisible = true
        RegisterWhitelist(this.binding,this)
        MobileNumberWhitelist(this)
        binding.includeWhitelist.lblWhtlstCancel.setOnClickListener {
//            val intent = Intent(this, LandingActivity::class.java)
//            startActivity(intent)

            overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down)
            finish()
        }
    }

}
