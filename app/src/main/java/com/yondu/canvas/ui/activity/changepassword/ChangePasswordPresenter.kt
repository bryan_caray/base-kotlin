package com.yondu.canvas.ui.activity.changepassword

import com.yondu.canvas.global.mvp.MvpPresenter
import com.yondu.canvas.model.remote.body.UserBody
import com.yondu.canvas.model.remote.response.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import retrofit2.HttpException
import java.net.SocketTimeoutException

class ChangePasswordPresenter(view: ChangePasswordView) : MvpPresenter<ChangePasswordView>(view) {

    fun submitChange(tempPassword:CharSequence,password: CharSequence){
        var userBody = UserBody()
        userBody.password = password.toString()
        userBody.username = getUser()!!.email
        userBody.tempPassword = tempPassword.toString()

        //Process Registration Here
        var user: RealmResults<User> = realm.where(User::class.java).findAllAsync()

       api.postChangePassword(userBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

            .doOnError { error ->
                if(error is SocketTimeoutException){
                    view.onFailedChange("Socket Timeout")

                }else{
                    val httpError = error as HttpException
                    view.onFailedChange( httpError.body().message!!)
                }
            }
            .subscribe({response ->
                realm.executeTransaction {
                    realm.insertOrUpdate(response.data!!.user)
                    view.onSuccessChange(response.message!!)
                }},{})
    }

    private fun getUser(): User? {
        val realm = Realm.getDefaultInstance()
        return realm.where(User::class.java!!).findFirst()
    }
}