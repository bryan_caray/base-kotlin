package com.yondu.canvas.ui.dialog

import android.app.Activity
import android.graphics.drawable.Drawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.yondu.canvas.R
import com.yondu.canvas.databinding.DialogCountryDialsBinding
import com.yondu.canvas.global.mvp.MvpDialog
import com.yondu.canvas.model.local.CountryDial
import com.yondu.canvas.ui.adapter.CountryDialAdapter

class CountryDialsDialog(activity: Activity, private val countries: List<CountryDial>, private val listener: ItemListener)
    : MvpDialog<DialogCountryDialsBinding>(activity), CountryDialAdapter.ItemListener, SearchView.OnQueryTextListener {

    override var layoutId = R.layout.dialog_country_dials

    private lateinit var adapter: CountryDialAdapter



    override fun onViewCreated() {
        binding.svCountry.setOnQueryTextListener(this)
        binding.rvCountries.adapter = CountryDialAdapter(countries, this)
        binding.rvCountries.layoutManager = LinearLayoutManager(activity)

        adapter = binding.rvCountries.adapter as CountryDialAdapter
    }

    override fun onQueryTextChange(query: String): Boolean {
        adapter.items = countries.filter { it.name!!.contains(query, true) }.sortedBy { it.name }
        adapter.notifyDataSetChanged()
        return false
    }

    override fun onQueryTextSubmit(query: String): Boolean = false

    override fun onItemClicked(item: CountryDial) {
        listener.onItemSelected(item)

    }

    interface ItemListener {

        fun onItemSelected(item: CountryDial)
    }
}