package com.yondu.canvas.ui.activity.register

import android.content.Intent
import android.text.Editable
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityRegisterBinding
import com.yondu.canvas.global.module.register.RegisterAnyEmail
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.ui.activity.main.MainActivity
import com.yondu.canvas.ui.helper.MobileNumber

class RegisterActivity : MvpActivity<ActivityRegisterBinding,RegisterPresenter, RegisterView>(),
    RegisterView, RegisterAnyEmail.Handler{

    var isSuccess : Boolean = false


    override fun onFailedRegister(error: String) {
        closeDialogBox()
        openErrorBox(error,"Failed Register",null,"OK",true)

    }

    override fun onPositiveClicked() {
        closeDialogBox()
        if(isSuccess){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
        }
    }

    override fun onSuccessRegister(message:String) {
        isSuccess = true
        //MainActivity is Login
        closeDialogBox()
        openMessageBox(message,"Successful Register",null,"Ok",true)
    }

    override fun onSubmit(
        email: CharSequence,
        password: CharSequence,
        name: CharSequence,
        phoneNumber: CharSequence
    ) {
        openProgressBox()
        presenter.submitRegistration(email,password,name,phoneNumber)
    }

    override val presenter get() = RegisterPresenter(this)

    override val layoutId get() = R.layout.activity_register



    override fun onViewCreated() {
        binding.isVisible = true
        binding.isWhitelistVisible = false
        RegisterAnyEmail(binding.include, this)
        MobileNumber(this)
        binding.include.lblSignCancel.setOnClickListener {
//            val intent = Intent(this, LandingActivity::class.java)
//            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down)
            finish()
        }
//        TextInputSeparator(listOf(etCardNumber0, etCardNumber1, etCardNumber2, etCardNumber3), 4)
    }

}
