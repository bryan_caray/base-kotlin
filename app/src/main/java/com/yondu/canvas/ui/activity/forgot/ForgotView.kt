package com.yondu.canvas.ui.activity.forgot

import com.yondu.canvas.global.mvp.MvpView

interface ForgotView : MvpView.ActivityFragment {

    fun onForgotSuccess(message:String)
    fun onForgotFailed(error:String)

}