package com.yondu.canvas.ui.activity.splash

import com.yondu.canvas.global.mvp.MvpView

interface SplashView : MvpView.ActivityFragment{
    fun startLanding()
    fun startHome()
}