package com.yondu.canvas.ui.adapter

import androidx.core.text.PrecomputedTextCompat
import com.yondu.canvas.R
import com.yondu.canvas.databinding.AdapterPostBinding
import com.yondu.canvas.global.mvp.MvpRecyclerAdapter
import com.yondu.canvas.model.db.Post

class PostAdapter(items: List<Post>, private val listener: ItemListener)
    : MvpRecyclerAdapter<AdapterPostBinding,Post>(R.layout.adapter_post, items) {

    override fun onViewCreated(binding: AdapterPostBinding, item: Post) {
        binding.tvTitle.setTextFuture(
            PrecomputedTextCompat.getTextFuture(
                item.title,
                binding.tvTitle.textMetricsParamsCompat,
                /*optional custom executor*/ null))
        binding.tvAuthor.setTextFuture(
            PrecomputedTextCompat.getTextFuture(
                item.userId.toString(),
                binding.tvAuthor.textMetricsParamsCompat,
                /*optional custom executor*/ null))
        binding.tvDescription.setTextFuture(
            PrecomputedTextCompat.getTextFuture(
                item.body,
                binding.tvDescription.textMetricsParamsCompat,
                /*optional custom executor*/ null))

        binding.root.setOnClickListener {
            listener.onItemClicked(item)
        }
    }


    interface ItemListener {

        fun onItemClicked(item: Post)
    }
}