package com.yondu.canvas.ui.widget

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.yondu.canvas.R

class PagerIndicatorView : LinearLayoutCompat, ViewPager.OnPageChangeListener {

    private val pages: ArrayList<AppCompatImageView> = ArrayList()

    private lateinit var viewPager: ViewPager

    private var resource = 0

    private var size = 0

    private var scale = 0f

    private var selectedTint = 0

    private var unselectedTint = 0

    private var selected = 0

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.PagerIndicatorView, 0, 0)
        resource = attributes.getResourceId(R.styleable.PagerIndicatorView_resource, R.drawable.ic_pager_indicator)
        size = attributes.getDimensionPixelSize(R.styleable.PagerIndicatorView_size, 48)
        scale = attributes.getFloat(R.styleable.PagerIndicatorView_scale, 1.5F)
        selectedTint = attributes.getColor(R.styleable.PagerIndicatorView_selectedTint, ContextCompat.getColor(context, R.color.colorAccent))
        unselectedTint = attributes.getColor(R.styleable.PagerIndicatorView_unselectedTint, ContextCompat.getColor(context, R.color.colorPrimary))
    }

    init {
        orientation = HORIZONTAL
    }

    fun attach(viewPager: ViewPager) {
        this.viewPager = viewPager
        viewPager.addOnPageChangeListener(this)

        for (index in 0 until viewPager.adapter!!.count)
            addView(createPage(index))

        setSelectedPage(selected)
    }

    private fun createPage(index: Int): AppCompatImageView {
        val params = LayoutParams(size, size)
        val page = AppCompatImageView(context)
        page.setImageResource(resource)
        page.setColorFilter(unselectedTint)
        page.setOnClickListener { viewPager.setCurrentItem(index, true) }
        page.layoutParams = params
        pages.add(page)
        return page
    }

    private fun setSelectedPage(index: Int) {
        pages[selected].animate().scaleX(1f).scaleY(1f).start()
        pages[selected].setColorFilter(unselectedTint)

        pages[index].animate().scaleX(scale).scaleY(scale).start()
        pages[index].setColorFilter(selectedTint)

        selected = index
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        setSelectedPage(position)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }
}