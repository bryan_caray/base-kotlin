package com.yondu.canvas.ui.activity.splash


import com.yondu.canvas.global.mvp.MvpPresenter
import com.yondu.canvas.global.util.PreferenceUtil

class SplashPresenter(view: SplashView) : MvpPresenter<SplashView>(view) {
    fun checkIsLoggedIn() {
    if(PreferenceUtil.instance.hasUser)view.startHome()
    else view.startLanding()
    }
}