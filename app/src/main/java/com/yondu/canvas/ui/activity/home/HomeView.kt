package com.yondu.canvas.ui.activity.home

import com.yondu.canvas.global.mvp.MvpView

interface HomeView : MvpView.ActivityFragment {

    fun successLogout(){}
    fun failLogout(){}
    fun onSuccessFetch(){}
    fun onFailedFetch(error:String){}
    fun showChangePassword(){}
}