package com.yondu.canvas.ui.activity.home.fragment

import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yondu.canvas.R
import com.yondu.canvas.databinding.FragmentHomeBinding
import com.yondu.canvas.global.mvp.MvpFragment
import com.yondu.canvas.model.db.Post
import com.yondu.canvas.ui.activity.home.HomePresenter
import com.yondu.canvas.ui.activity.home.HomeView
import com.yondu.canvas.ui.adapter.PostAdapter

class FragmentHome : MvpFragment<FragmentHomeBinding, HomePresenter, HomeView>(), HomeView,PostAdapter.ItemListener {
    override fun onSuccessFetch() {
    }

    override fun onFailedFetch(error: String) {
        super.onFailedFetch(error)
    }
    override fun showChangePassword() {
      }

    override fun onItemClicked(item: Post) {
    Toast.makeText(this.context,item.title,Toast.LENGTH_SHORT).show()
       }

    override val presenter = HomePresenter(this)

    override val layoutId = R.layout.fragment_home

    override fun onViewCreated() {
        val llm = LinearLayoutManager(this.context)
        llm.orientation = RecyclerView.VERTICAL
        binding.rvPost.layoutManager =llm
        binding.rvPost.adapter = PostAdapter(presenter.getRealmPost(), this)
    }

}