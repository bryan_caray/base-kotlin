package com.yondu.canvas.ui.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet

open class TextInputMobileNumber : TextInputClickableDrawable {

    private val listeners: ArrayList<TextWatcher> = ArrayList()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onSelectionChanged(start: Int, end: Int) {
        if (tag != null) {
            val len = (tag as CharSequence).length
            if (start < len || len > end)
                setSelection(len, len)
        }

        super.onSelectionChanged(start, end)
    }

    override fun addTextChangedListener(watcher: TextWatcher) {
        listeners.add(watcher)
        return super.addTextChangedListener(watcher)
    }

    override fun removeTextChangedListener(watcher: TextWatcher) {
        listeners.removeAt(listeners.indexOf(watcher))
        return super.removeTextChangedListener(watcher)
    }

    fun clearTextChangedListeners() {
        listeners.forEach { removeTextChangedListener(it) }
        listeners.clear()
    }

    fun setStartDrawable(drawable: Drawable) {
        return super.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
    }

    fun setPrefix(drawable: Drawable, prefix: CharSequence) {
        setStartDrawable(drawable)

        clearTextChangedListeners()

        tag = prefix
        setText(prefix)
        setSelection(prefix.length)

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable) {
                if (text.length < prefix.length)
                    setText(prefix)
            }

            override fun beforeTextChanged(source: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(source: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    fun getPhoneNumber(): CharSequence {
        val prefix = tag as CharSequence
        return "${text!!.substring(0, prefix.length - 1)}${text!!.substring(prefix.length, text!!.length)}"
    }
    fun isEmpty():Boolean{
        val prefix = tag as CharSequence
        return text!!.substring(prefix.length, text!!.length).isNullOrBlank()
    }
}