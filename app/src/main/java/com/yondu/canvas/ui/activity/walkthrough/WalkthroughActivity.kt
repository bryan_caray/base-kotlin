package com.yondu.canvas.ui.activity.walkthrough


import android.content.Intent
import android.util.Log
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityWalkthroughBinding
import com.yondu.canvas.global.module.walkthrough.Walkthrough
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.global.util.PreferenceUtil
import com.yondu.canvas.ui.activity.home.HomeActivity

class WalkthroughActivity: MvpActivity<ActivityWalkthroughBinding, WalkthroughPresenter, WalkthroughView>(),
    WalkthroughView,Walkthrough.Handler {

    override fun onDonePressed() {
    done()
    }

    private fun done() {
        PreferenceUtil.instance.hasWalkthrough = true
        var intent = Intent(this, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
    override fun onFirstPage() {
        binding.dnBtn.text = "Skip"
        binding.bckBtn.isEnabled = false
        super.onFirstPage()
    }

    override fun onLastPage() {
        binding.dnBtn.text = "Done"
        binding.bckBtn.isEnabled = true
        super.onLastPage()
    }

    override fun onOtherPage() {
        binding.dnBtn.text = "Skip"
        binding.bckBtn.isEnabled = true
        super.onOtherPage()
    }

    override fun onViewCreated() {

        intent.putExtra("IS_SHARED_ELEMENT_TRANSITION_ENABLED", false)
        Walkthrough(binding,this)
        binding.dnBtn.setOnClickListener {
            done()
//            var pager =  binding.include.walkPager
//           var position = pager.currentItem
//            var size = pager.adapter!!.count-1
//
//            if(position == size){
//                done()
//            }
//            else{
//                pager.currentItem = position+1
//            }
        }

        binding.bckBtn.setOnClickListener {
            var pager =  binding.include.walkPager
            var position = pager.currentItem
            pager.currentItem = position-1
        }
}

override val presenter get() = WalkthroughPresenter(this)

    override val layoutId get() = R.layout.activity_walkthrough







}
