package com.yondu.canvas.ui.activity.home.fragment

import com.yondu.canvas.R
import com.yondu.canvas.databinding.FragmentNotificationBinding
import com.yondu.canvas.global.mvp.MvpFragment
import com.yondu.canvas.ui.activity.home.HomePresenter
import com.yondu.canvas.ui.activity.home.HomeView


class FragmentNotification: MvpFragment<FragmentNotificationBinding, HomePresenter, HomeView>(), HomeView {
    override val presenter = HomePresenter(this)

    override val layoutId = R.layout.fragment_notification
    override fun onViewCreated() {
    }
}