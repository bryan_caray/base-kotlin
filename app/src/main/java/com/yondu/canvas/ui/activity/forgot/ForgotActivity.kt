package com.yondu.canvas.ui.activity.forgot


import android.content.Intent
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityForgotBinding
import com.yondu.canvas.global.mvp.MvpActivity
import com.yondu.canvas.ui.activity.main.MainActivity

class ForgotActivity : MvpActivity<ActivityForgotBinding,ForgotPresenter, ForgotView>(),
    ForgotView{
    var isSuccess = false
    override fun onPositiveClicked() {
        if(isSuccess){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onForgotFailed(error : String) {
        closeDialogBox()
        openErrorBox(error,"Failed Forgot",null,"Ok",true)

    }

    override fun onForgotSuccess(message : String) {
        isSuccess = true
        closeDialogBox()
        openMessageBox(message,"Success Forgot Password",null,"Ok",true)

    }

    override val presenter get() = ForgotPresenter(this)

    override val layoutId get() = R.layout.activity_forgot

    override fun onViewCreated() {
        val email = intent.getStringExtra("email")
        binding.tiEmail.setText(email)
        binding.tiEmail.setSelection(binding.tiEmail.text!!.length)
        binding.lblFgtCnl.setOnClickListener {
            finish()

        }
        binding.bProceed.setOnClickListener {
            openProgressBox()
            presenter.sendForgot(binding.tiEmail.text!!)
        }


    }


}
