package com.yondu.canvas.ui.activity.main

import com.yondu.canvas.R
import com.yondu.canvas.databinding.FragmentMainBinding
import com.yondu.canvas.global.mvp.MvpFragment

class MainFragment : MvpFragment<FragmentMainBinding, MainPresenter, MainView>(), MainView {
    override fun onFailedLogin(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccessLogin(first: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override val presenter = MainPresenter(this)

    override val layoutId = R.layout.fragment_main

    override fun onViewCreated() {

    }
}