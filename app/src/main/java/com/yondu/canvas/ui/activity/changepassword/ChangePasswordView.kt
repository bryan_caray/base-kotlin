package com.yondu.canvas.ui.activity.changepassword

import com.yondu.canvas.global.mvp.MvpView

interface ChangePasswordView: MvpView.ActivityFragment {
    fun onFailedChange(error: String)

    fun onSuccessChange(message: String)
}