package com.yondu.canvas.ui.activity.register

import com.yondu.canvas.global.mvp.MvpView

interface RegisterView : MvpView.ActivityFragment {
fun onSuccessRegister(message:String)
fun onFailedRegister(error:String)
}