package com.yondu.canvas.ui.activity.main

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import com.yondu.canvas.R
import com.yondu.canvas.databinding.ActivityMainBinding
import com.yondu.canvas.global.mvp.MvpActivity

import com.yondu.canvas.ui.activity.changepassword.ChangePasswordActivity
import com.yondu.canvas.ui.activity.forgot.ForgotActivity
import com.yondu.canvas.ui.activity.walkthrough.WalkthroughActivity
import com.yondu.canvas.ui.helper.EmailPassword
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.yondu.canvas.global.module.loginnormal.EmailPasswordNormal
import com.yondu.canvas.global.util.PreferenceUtil
import com.yondu.canvas.ui.activity.home.HomeActivity
import kotlinx.android.synthetic.main.activity_splash.*



class MainActivity : MvpActivity<ActivityMainBinding, MainPresenter, MainView>(), MainView, EmailPassword.Handler,EmailPasswordNormal.Handler{


    override fun onPositiveClicked() {
        //Do nothing
        closeDialogBox()
    }

    override fun onFailedLogin(message: String) {
        closeDialogBox()
        openErrorBox(message,"Failed Login",null,"Ok",true)

    }

    override fun onSuccessLogin(first: Boolean) {
        var intent = Intent(this, WalkthroughActivity::class.java)
        if(PreferenceUtil.instance.hasWalkthrough){
            intent = Intent(this, HomeActivity::class.java)
        }
        if(first){
            var bundle = Bundle()
            intent = Intent(this, ChangePasswordActivity::class.java)
            bundle.putBoolean("isFirst",true)
            intent.putExtras(bundle)
        }

        closeDialogBox()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        startActivity(intent)
        finish()
    }

    override fun onForgotLogin(email: CharSequence) {
        val intent = Intent(this, ForgotActivity::class.java)
        intent.putExtra("email", email)
        startActivity(intent)

    }

    override fun onCancelLogin() {
        onBackPressed()
    }

    override fun onSubmit(email: CharSequence, password: CharSequence) {
        openProgressBox()
        presenter.submitLogin(email,password)
    }

    override val presenter get() = MainPresenter(this)

    override val layoutId get() = R.layout.activity_main

    override fun onViewCreated() {

        binding.isVisible = false
        binding.isNormalVisible = true
        supportPostponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgLogo.transitionName = "image_transition"
        }
        Glide.with(this)
            .load(getDrawable(R.drawable.logo))
            .dontAnimate()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }
            })
            .into(imgLogo)
        EmailPassword(binding, this)
        EmailPasswordNormal(binding,this)
    }
}
