package com.yondu.canvas.ui.activity.login_owasp

import com.yondu.canvas.R
import com.yondu.canvas.databinding.FragmentMainBinding
import com.yondu.canvas.global.mvp.MvpFragment

class LoginOwaspFragment : MvpFragment<FragmentMainBinding, LoginOwaspPresenter, LoginOwaspView>(), LoginOwaspView {
    override fun onFailedLogin(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccessLogin(first: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override val presenter = LoginOwaspPresenter(this)

    override val layoutId = R.layout.fragment_main

    override fun onViewCreated() {

    }
}