package com.yondu.canvas.ui.activity.landing

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.yondu.canvas.global.mvp.MvpView

interface LandingView : MvpView.ActivityFragment {
    fun successGoogleLogin(account: GoogleSignInAccount?)
    fun failedGoogleLogin(e:String)
}