package com.yondu.canvas.ui.activity.main

import com.yondu.canvas.global.mvp.MvpView

interface MainView : MvpView.ActivityFragment{
    fun onSuccessLogin(first: Boolean)
    fun onFailedLogin(message:String)

}