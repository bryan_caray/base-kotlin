package com.yondu.canvas.ui.adapter

import android.annotation.SuppressLint
import com.bumptech.glide.Glide
import com.yondu.canvas.R
import com.yondu.canvas.databinding.AdapterCountryDialBinding
import com.yondu.canvas.global.mvp.MvpRecyclerAdapter
import com.yondu.canvas.global.util.ResourceUtil
import com.yondu.canvas.model.local.CountryDial

class CountryDialAdapter(items: List<CountryDial>, private val listener: ItemListener)
    : MvpRecyclerAdapter<AdapterCountryDialBinding,CountryDial>(R.layout.adapter_country_dial, items) {

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(binding: AdapterCountryDialBinding, item: CountryDial) {
        Glide.with(binding.root)
            .load(ResourceUtil.getFlagDrawable(binding.root.context, item.code!!))
            .placeholder(ResourceUtil.getDefaultFlagRes)
            .into(binding.ivFlag)

        binding.tvCountry.text = item.name
        binding.tvPrefix.text = "(${item.prefix})"
        binding.root.setOnClickListener { listener.onItemClicked(item) }
       }



    interface ItemListener {

        fun onItemClicked(item: CountryDial)
    }
}