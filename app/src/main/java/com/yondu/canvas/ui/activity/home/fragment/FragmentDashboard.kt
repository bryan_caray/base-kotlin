package com.yondu.canvas.ui.activity.home.fragment

import com.yondu.canvas.R
import com.yondu.canvas.ui.activity.home.HomePresenter
import com.yondu.canvas.ui.activity.home.HomeView
import com.yondu.canvas.databinding.FragmentDashboardBinding
import com.yondu.canvas.global.mvp.MvpFragment

class FragmentDashboard : MvpFragment<FragmentDashboardBinding,HomePresenter, HomeView>(), HomeView {
    override val presenter = HomePresenter(this)

    override val layoutId = R.layout.fragment_dashboard
    override fun onViewCreated() {
    }


}