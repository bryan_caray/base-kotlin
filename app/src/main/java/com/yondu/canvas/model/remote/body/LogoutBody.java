package com.yondu.canvas.model.remote.body;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ralph Orden on 1/18/2018.
 */

public class LogoutBody {

    @SerializedName("truck_personnel_team_uuid")
    private String teamUUID;

    public String getTeamUUID() {
        return teamUUID;
    }

    public void setTeamUUID(String teamUUID) {
        this.teamUUID = teamUUID;
    }
}
