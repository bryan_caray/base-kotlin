package com.yondu.canvas.model.remote.body


class UserBody {

    var username: String? = null
    var tempPassword: String? = null
    var password: String? = null
    var confirmPassword: String? = null
}
