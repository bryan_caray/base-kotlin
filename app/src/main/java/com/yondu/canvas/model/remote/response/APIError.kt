package com.yondu.canvas.model.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class APIError {

    @Expose
    @SerializedName("message")
    var message: String? = null
}
