package com.yondu.canvas.model.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Data {

    @SerializedName("user")
    @Expose
    var user: User? = null
    @SerializedName("token")
    @Expose
    var token: String? = null
    @SerializedName("isFirst")
    @Expose
    var first: Boolean? = null
}
