package com.yondu.canvas.model.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserResponse {

    @SerializedName("message")
    @Expose
    var message: String? = null


    @SerializedName("data")
    @Expose
    var data: Data? = null

}
