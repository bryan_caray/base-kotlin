package com.yondu.canvas.model.remote.utils

import androidx.databinding.InverseMethod
import com.yondu.canvas.global.Constant
import java.text.SimpleDateFormat
import java.util.*

object TextUtil {

    @JvmStatic
    @InverseMethod("stringToDate")
    fun dateToString(date: Date): String =
        formatDate(date, Constant.DATE_FORMAT)

    @JvmStatic
    fun stringToDate(date: String): Date =
        parseDate(date, Constant.DATE_FORMAT)

    @JvmStatic
    @InverseMethod("stringToTime")
    fun timeToString(date: Date): String =
        formatDate(date, Constant.TIME_FORMAT)

    @JvmStatic
    fun stringToTime(date: String): Date =
        parseDate(date, Constant.TIME_FORMAT)

    @JvmStatic
    @InverseMethod("stringToDateTime")
    fun dateTimeToString(date: Date): String =
        formatDate(date, Constant.DATE_TIME_FORMAT)

    @JvmStatic
    fun stringToDateTime(date: String): Date =
        parseDate(date, Constant.DATE_TIME_FORMAT)

    private fun formatDate(date: Date, format: String): String =
        SimpleDateFormat(format, Locale.getDefault()).format(date)

    private fun parseDate(date: String, format: String): Date =
        SimpleDateFormat(format, Locale.getDefault()).parse(date)
}