package com.yondu.canvas.model

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import java.io.Serializable

interface BaseModel : Observable, Serializable {

    companion object {

        @Transient
        @get:Synchronized
        private val registry = PropertyChangeRegistry()
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        registry.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        registry.add(callback)
    }
}
