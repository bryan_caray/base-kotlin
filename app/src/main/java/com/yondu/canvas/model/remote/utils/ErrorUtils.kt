package com.yondu.canvas.model.remote.utils

import com.yondu.canvas.global.api.ApiClient
import com.yondu.canvas.model.remote.response.APIError
import retrofit2.Response



object ErrorUtils {
    fun parseError(response: Response<*>): APIError? {
        val a = object : Annotation{}
        val converter = ApiClient.instance.responseBodyConverter<APIError>(APIError::class.java, arrayOf(a))
        return  converter.convert(response.errorBody())


    }
}