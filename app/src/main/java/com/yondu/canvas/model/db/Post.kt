package com.yondu.canvas.model.db

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class Post : RealmObject() {

    @PrimaryKey
    @Expose
    @SerializedName("id")
    open var id: Long = 0

    @Expose
    @SerializedName("userId")
    open var userId: Int = 0

    @Expose
    @SerializedName("title")
    open var title: String = ""

    @Expose
    @SerializedName("body")
    open var body: String = ""
}