package com.yondu.canvas.model.local

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class CountryDial {

    @Expose
    @SerializedName("name")
    open var name: String? = null

    @Expose
    @SerializedName("prefix")
    open var prefix: String? = null

    @Expose
    @SerializedName("code")
    open var code: String? = null
}