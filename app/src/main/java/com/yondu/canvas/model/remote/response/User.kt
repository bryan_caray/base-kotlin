package com.yondu.canvas.model.remote.response;

import androidx.databinding.Bindable
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yondu.canvas.model.BaseModel;
import io.realm.RealmModel;

open class User : BaseModel, RealmModel {

    @Bindable
    @SerializedName("email")
    @Expose
    open var email: String? = null
    @SerializedName("createdAt")
    @Expose
    open var createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    open var updatedAt: String? = null
    @SerializedName("id")
    @Expose
    open var id: Int? = null

    @Expose
    open var name: String? = null


}
