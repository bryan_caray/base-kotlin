package com.yondu.canvas.global.api

import com.yondu.canvas.global.mvp.MvpView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class ApiCallback<T>(protected val handler: MvpView.ActivityFragment) : Callback<T> {

    protected open var body: T? = null

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) body = response.body()
    }

    override fun onFailure(call: Call<T>, throwable: Throwable) {
        
    }
}