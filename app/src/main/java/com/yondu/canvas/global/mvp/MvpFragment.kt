package com.yondu.canvas.global.mvp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.yondu.canvas.ui.activity.home.fragment.FragmentHome

abstract class MvpFragment<B : ViewDataBinding, P : MvpPresenter<V>, V : MvpView.ActivityFragment> : Fragment(),
    MvpView.ActivityFragment {

    protected val TAG = javaClass.simpleName

    protected lateinit var binding: B

    protected abstract val presenter: P

    override val hasBusEvent: Boolean get() = false

    companion object {
        fun<T :MvpFragment<*,*,*>> newInstance(clazz: Class<T>):T{
            return newInstance(clazz,null)
        }
        fun<T :MvpFragment<*,*,*>> newInstance(clazz: Class<T>,bundle: Bundle?):T{
            val newInstance = clazz.newInstance()
            newInstance.setArguments(bundle)
            return newInstance
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreated()
    }


}