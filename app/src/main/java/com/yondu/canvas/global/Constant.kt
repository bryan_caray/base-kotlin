package com.yondu.canvas.global

object Constant {

    const val EMPTY_STRING = ""
    const val EMPTY_NUMBER = -1
    const val DATE_PARSE = "yyyy-MM-dd HH:mm:ss"
    const val DATE_FORMAT = "MMMM dd, yyyy"
    const val TIME_FORMAT = "hh:mm aa"
    const val DATE_TIME_FORMAT = "$DATE_FORMAT $TIME_FORMAT"

}