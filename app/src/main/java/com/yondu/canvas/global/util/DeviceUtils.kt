package com.yondu.canvas.global.util

import android.content.Context
import android.net.ConnectivityManager

class DeviceUtils(private val mContext: Context) {

    fun hasNetworkConnection(): Boolean {
        return IsWifiActive() || IsPacketDataActive()
    }

    private fun IsWifiActive(): Boolean {
        val connectivity = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.activeNetworkInfo
            if (info != null)
                if (info.type == ConnectivityManager.TYPE_WIFI || info.type == ConnectivityManager.TYPE_MOBILE) {
                    if (info.isConnected)
                        return true
                }
        }
        return false
    }

    private fun IsPacketDataActive(): Boolean {
        val connectivity = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.activeNetworkInfo
            if (info != null) {
                if (info.type == ConnectivityManager.TYPE_MOBILE) {
                    if (info.isConnected)
                        return true
                }
            }
        }
        return false
    }

}