package com.yondu.canvas.global.module.register

import android.text.Editable
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.yondu.canvas.databinding.RegisteranyemailBinding
import com.yondu.canvas.global.util.TextUtil


class RegisterAnyEmail(binding: RegisteranyemailBinding, handler: Handler) {

    private val txtRegName = binding.txtRegName
    private val tilRegName = binding.tilName

    private val txtRegPhone = binding.txtRegPhone
    private val tilRegPhone = binding.tilRegPhone

    private val txtRegEmail = binding.txtRegEmail
    private val tilRegEmail = binding.tilRegEmail

    private val txtRegPass = binding.txtRegPass
    private val tilRegPass = binding.tilRegPass

    private val txtRegCnfrmPass = binding.txtRegCnfrmPass
    private val tilRegCnfrmPass = binding.tilRegCnfrmPass

    private val btnRegister = binding.btnSubmitRegister
    init {

        btnRegister.setOnClickListener{
            if(txtRegName.text.isNullOrBlank()){
                TextUtil.setError(txtRegName, tilRegName, "${txtRegName.hint} is empty")
            }
            else if(txtRegPhone.isEmpty()){
                TextUtil.setError(txtRegPhone, tilRegPhone, "${txtRegPhone.hint} is empty")
            }
            else if (validateEmail(txtRegEmail,tilRegEmail)
                && validatePassword(txtRegPass,tilRegPass)
                && validatePassword(txtRegCnfrmPass,tilRegCnfrmPass)
                && validateIfPasswordMatched()   ){
                handler.onSubmit(txtRegEmail.text!!,txtRegCnfrmPass.text!!,txtRegName.text!!,txtRegPhone.getPhoneNumber())
            }

        }
    }


    fun validatePassword(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val password = input.text!!

        when {
            password.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }
        }

        return true
    }

    private fun validateEmail(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val email = input.text!!

        when {
            email.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }

            !TextUtil.isEmail(email) -> {
                TextUtil.setError(input, layout, "${input.hint} is invalid")
                return false
            }
        }

        return true
    }

    fun validateIfPasswordMatched(): Boolean {
        var s1:String = txtRegPass.text.toString()
        var s2:String = txtRegCnfrmPass.text.toString()
        if(!s1.equals(s2)){
        TextUtil.setError(txtRegCnfrmPass,tilRegCnfrmPass,"${txtRegPass.hint} and ${txtRegCnfrmPass.hint}  does not Matched")
        return false
    }
        return true
    }




interface Handler {

    fun onSubmit(
        email: CharSequence,
        password: CharSequence,
        name: CharSequence,
        phoneNumber: CharSequence
    )
}

}