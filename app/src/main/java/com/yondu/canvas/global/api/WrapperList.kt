package com.yondu.canvas.global.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class WrapperList<T> : WrapperBase() {

    @Expose
    @SerializedName("data")
    open val data: List<T>? = null
}