package com.yondu.canvas.global.api

import android.app.Activity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.yondu.canvas.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class GoogleClient {

    companion object {

        @get:Synchronized
        lateinit var mGoogleSignInClient: GoogleSignInClient

        private val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("485676733366-t3j9huo1bqtga2kt8ujvmm34q0l165p9.apps.googleusercontent.com")
            .requestEmail()
            .build()

        fun createInstance(activity: Activity) {
            mGoogleSignInClient = GoogleSignIn.getClient(activity, gso)

        }
    }
}
