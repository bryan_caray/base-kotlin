package com.yondu.canvas.global.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class WrapperObject<T> : WrapperBase() {

    @Expose
    @SerializedName("data")
    open var data: T? = null
}