package com.yondu.canvas.global.mvp

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

abstract class MvpPagerAdapter<T>(private val layouts: List<T>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any = layouts[position]!!

    override fun isViewFromObject(view: View, obj: Any): Boolean = view === obj as View

    override fun getCount(): Int = layouts.size
}