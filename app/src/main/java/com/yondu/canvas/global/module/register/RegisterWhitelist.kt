package com.yondu.canvas.global.module.register

import android.text.Editable
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.yondu.canvas.databinding.ActivityRegisterBinding
import com.yondu.canvas.global.util.TextUtil


class RegisterWhitelist(binding: ActivityRegisterBinding, handler: Handler) {
    private val txtRegEmail = binding.includeWhitelist.txtWhtRegEmail
    private val tilRegEmail = binding.includeWhitelist.tilWhtRegEmail

    private val txtRegName = binding.includeWhitelist.txtWhtRegName
    private val tilRegName = binding.includeWhitelist.tilWhtName

    private val txtRegPhone = binding.includeWhitelist.txtWhtRegPhone
    private val tilRegPhone = binding.includeWhitelist.tilWhtRegPhone

    private val btnRegister = binding.includeWhitelist.btnWhtlstRegister
    init {
        btnRegister.setOnClickListener{
            when {
                txtRegName.text.isNullOrBlank() -> TextUtil.setError(txtRegName, tilRegName, "${txtRegName.hint} is empty")
                txtRegPhone.isEmpty() -> TextUtil.setError(txtRegPhone, tilRegPhone, "${txtRegPhone.hint} is empty")
                validateEmail(txtRegEmail,tilRegEmail) -> handler.onSubmit(txtRegEmail.text!!,txtRegName!!.text!!,txtRegPhone.getPhoneNumber())
            }

        }
    }
    private fun validateEmail(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val email = input.text!!

        when {
            email.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }

            !TextUtil.isEmail(email) -> {
                TextUtil.setError(input, layout, "${input.hint} is invalid")
                return false
            }
        }

        return true
    }

interface Handler {

    fun onSubmit(email: CharSequence, name: Editable, phoneNumber: CharSequence)
}

}