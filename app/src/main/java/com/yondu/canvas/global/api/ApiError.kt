package com.yondu.canvas.global.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class ApiError {

    @SerializedName("message")
    @Expose
    open var message: String? = null
}