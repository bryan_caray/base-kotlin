package com.yondu.canvas.global.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class WrapperBase {

    @Expose
    @SerializedName("status")
    open val status: Int? = null

    @Expose
    @SerializedName("message")
    open val message: String? = null

    @Expose
    @SerializedName("code")
    open val code: Int? = null

    fun isSuccess(): Boolean = status == 1
}