package com.yondu.canvas.global.mvp

import com.yondu.canvas.global.api.ApiClient
import com.yondu.canvas.global.api.ApiError
import com.yondu.canvas.global.api.ApiService
import io.realm.Realm
import retrofit2.HttpException

abstract class MvpPresenter<V : MvpView.ActivityFragment>(protected val view: V): MvpView.Presenter {

    protected val TAG = javaClass.simpleName

    override val realm: Realm = Realm.getDefaultInstance()

    override val api: ApiService = ApiClient.instance.service

    protected fun HttpException.body(): ApiError = response().errorBody().let {
        ApiClient.instance.gson.fromJson(it?.string(), ApiError::class.java)
    }

    open fun destroy() {
        realm.close()
    }
}