package com.yondu.canvas.global

import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.yondu.canvas.BuildConfig
import com.yondu.canvas.R
import com.yondu.canvas.global.api.ApiClient
import com.yondu.canvas.global.util.PreferenceUtil
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration

class BaseApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        createRealmInstance()
        ApiClient.createInstance()
        PreferenceUtil.createInstance(this)

        if (!BuildConfig.DEBUG)
            Fabric.with(this, Crashlytics())
    }

    private fun createRealmInstance() {
        val defaultVersion = 1L

        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().run {
            name("${getString(R.string.app_name)}.realm")
            schemaVersion(defaultVersion)
            if (BuildConfig.DEBUG) deleteRealmIfMigrationNeeded()
            else migration { realm, oldVersion, newVersion ->
                val schema = realm.schema
            }
            build()
        })
    }
}