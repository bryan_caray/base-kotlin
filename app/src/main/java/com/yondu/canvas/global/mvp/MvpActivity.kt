package com.yondu.canvas.global.mvp

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import com.yondu.canvas.BR
import com.yondu.canvas.R
import com.yondu.canvas.global.util.DeviceUtils
import com.yondu.canvas.ui.dialog.MessageDialog
import com.yondu.canvas.ui.dialog.ProgressDialog
import org.greenrobot.eventbus.EventBus

abstract class MvpActivity<B : ViewDataBinding, P : MvpPresenter<V>, V : MvpView.ActivityFragment> : AppCompatActivity(),
    MvpView.ActivityFragment, MessageDialog.OnClickListener {

    protected val TAG = javaClass.simpleName

    protected lateinit var binding: B

    protected open val fragmentViewId = 0

    protected abstract val presenter: P

    override val hasBusEvent: Boolean get() = false

    private var dialogBox: AlertDialog? = null

    private var backPressed = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setVariable(BR.handler,this)
        onViewCreated()
    }




    protected fun openMessageBox(message: CharSequence, title: CharSequence? = null, icon: Drawable? = null,
                                 positive: CharSequence = "CLOSE", isStrong: Boolean = false) {
        val dialog = MessageDialog(this, this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.NONE, icon, title)
        dialog.setMessage(message)
        dialog.setPositiveButton(positive)
        openDialogBox(dialog.create())
    }

    protected fun openSuccessBox(message: CharSequence, title: CharSequence? = null, icon: Drawable? = null,
                                 positive: CharSequence = "CLOSE", isStrong: Boolean = false) {
        val dialog = MessageDialog(this, this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.SUCCESS, icon, title)
        dialog.setMessage(message)
        dialog.setPositiveButton(positive)
        openDialogBox(dialog.create())
    }

    protected fun openErrorBox(message: CharSequence, title: CharSequence? = null, icon: Drawable? = null,
                               positive: CharSequence = "CLOSE", isStrong: Boolean = false) {
        val dialog = MessageDialog(this, this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.ERROR, icon, title)
        dialog.setMessage(message)
        dialog.setPositiveButton(positive)
        openDialogBox(dialog.create())
    }

    protected fun openWarningBox(message: CharSequence, title: CharSequence? = null, icon: Drawable? = null,
                                 positive: CharSequence = "CLOSE", isStrong: Boolean = false) {
        val dialog = MessageDialog(this, this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.WARNING, icon, title)
        dialog.setMessage(message)
        dialog.setPositiveButton(positive)
        openDialogBox(dialog.create())
    }

    protected fun openConfirmBox(message: CharSequence, title: CharSequence? = null, icon: Drawable? = null,
                                 positive: CharSequence = "CONFIRM", negative: CharSequence = "CANCEL", isStrong: Boolean = false) {
        val dialog = MessageDialog(this, this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.CONFIRM, icon, title)
        dialog.setMessage(message)
        dialog.setPositiveButton(positive)
        dialog.setNegativeButton(negative)
        openDialogBox(dialog.create())
    }

    protected fun openConfirmBox(message: CharSequence, title: CharSequence? = null, icon: Drawable? = null,
                                 positive: CharSequence = "CONFIRM", negative: CharSequence = "CANCEL", neutral: CharSequence = "NEUTRAL",
                                 isStrong: Boolean = false) {
        val dialog = MessageDialog(this, this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.CONFIRM, icon, title)
        dialog.setMessage(message)
        dialog.setPositiveButton(positive)
        dialog.setNegativeButton(negative)
        dialog.setNeutralButton(neutral)
        openDialogBox(dialog.create())
    }

    protected fun openProgressBox(message: CharSequence  = "Please wait...", isStrong: Boolean = true) {
        val dialog = ProgressDialog(this)
        dialog.initView()
        dialog.isStrong(isStrong)
        dialog.setTitle(MvpDialog.Type.NONE, null, null)
        dialog.setMessage(message)
        openDialogBox(dialog.create())
    }

    fun hasNetworkConnection(): Boolean {
        val deviceUtils = DeviceUtils(this)
        return deviceUtils.hasNetworkConnection()
    }

    fun noInternetConnection(){
        openErrorBox(getString(R.string.error_no_internet_found),null,null,"Ok",true)
    }

    private fun openDialogBox(dialog : AlertDialog) {
        if (dialogBox == null) {
            dialogBox = dialog
            dialogBox!!.setOnDismissListener {
                dialogBox = null
            }
            dialogBox!!.show()
        }
    }

    protected fun closeDialogBox() {
        if(dialogBox!=null){
            dialogBox!!.dismiss()
            dialogBox = null
        }

    }


    protected fun openFragment(fragment: MvpFragment<*, *, *>, backStack: String) {
        supportFragmentManager.also { manager ->
            manager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).apply {

                manager.fragments.forEach { fragment ->
                    if (fragment.isVisible) hide(fragment)
                }

                manager.findFragmentByTag(backStack)?.let { fragment ->
                    show(fragment)
                } ?: run {
                    add(fragmentViewId, fragment, backStack)
                }

            }.commitNow()
        }
    }

    protected fun showSnackbar(message: CharSequence, duration: Int) {
        Snackbar.make(binding.root, message, duration).show()
    }

    protected fun showToast(message: CharSequence, duration: Int) {
        Toast.makeText(baseContext, message, duration).show()
    }

    protected fun confirmExit() {
        if (backPressed + 2000L > System.currentTimeMillis()) super.onBackPressed()
        else showToast("Press back again to exit", Toast.LENGTH_SHORT)
        backPressed = System.currentTimeMillis()
    }

    override fun onStart() {
        super.onStart()
        if (hasBusEvent) EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        if (hasBusEvent) EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }
}