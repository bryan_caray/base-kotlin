package com.yondu.canvas.global.module.loginnormal

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.ViewPager
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.yondu.canvas.databinding.ActivityMainBinding
import com.yondu.canvas.global.mvp.MvpPagerAdapter
import com.yondu.canvas.global.util.TextUtil
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.login_email_password.view.*

class EmailPasswordNormal(binding: ActivityMainBinding, handler: Handler) {

    private val continer = binding.include2.loginContainer

    init {

        continer.bSubmit.setOnClickListener {
            if (validatePassword(continer.tiPassword, continer.ilPassword))
                handler.onSubmit(continer.tiEmail.text!!, continer.tiPassword.text!!)
        }
        continer.bForgot.setOnClickListener {
            handler.onForgotLogin(continer.tiEmail.text.toString())
        }
        continer.lblLoginCancel.setOnClickListener {
            handler.onCancelLogin()
        }
    }

    private fun validateEmail(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val email = input.text!!

        when {
            email.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }

            !TextUtil.isEmail(email) -> {
                TextUtil.setError(input, layout, "${input.hint} is invalid")
                return false
            }
        }

        return true
    }

    private fun validatePassword(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val password = input.text!!

        when {
            password.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }
        }

        return true
    }

    interface Handler {
        fun onCancelLogin()
        fun onSubmit(email: CharSequence, password: CharSequence)
        fun onForgotLogin(email: CharSequence)
    }
}