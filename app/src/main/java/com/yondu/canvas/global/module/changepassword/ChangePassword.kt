package com.yondu.canvas.global.module.changepassword

import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.yondu.canvas.databinding.ChangePasswordBinding
import com.yondu.canvas.global.util.TextUtil

class ChangePassword(binding: ChangePasswordBinding, handler: Handler) {

    private val lblCancel = binding.lblChngCnl

    private val txtTempPass = binding.txtPassTemp
    private val tilTempPass = binding.tilTempPass

    private val txtNewPass = binding.txtNewPass
    private val tilNewPass  = binding.tilPassNew

    private val txtCnfrmPass = binding.txtCnfrmPass
    private val tilCnfrmPass = binding.tilCnfrmPass

    private val btnChange = binding.btnSubmit
    init {
        btnChange.setOnClickListener{
            if (
                validatePassword(txtTempPass,tilTempPass)
                && validatePassword(txtNewPass,tilNewPass)
                && validatePassword(txtCnfrmPass,tilCnfrmPass)
                && validateIfPasswordMatched()   ){
                handler.onSubmit(txtTempPass.text!!,txtNewPass.text!!)
            }

        }
        lblCancel.setOnClickListener { handler.onCancel() }
    }


    fun validatePassword(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val password = input.text!!

        when {
            password.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }
        }

        return true
    }

    private fun validateEmail(input: TextInputEditText, layout: TextInputLayout): Boolean {
        val email = input.text!!

        when {
            email.isEmpty() -> {
                TextUtil.setError(input, layout, "${input.hint} is empty")
                return false
            }

            !TextUtil.isEmail(email) -> {
                TextUtil.setError(input, layout, "${input.hint} is invalid")
                return false
            }
        }

        return true
    }

    fun validateIfPasswordMatched(): Boolean {
        var s1:String = txtNewPass.text.toString()
        var s2:String = txtCnfrmPass.text.toString()
        if(!s1.equals(s2)){
            TextUtil.setError(txtCnfrmPass,tilCnfrmPass,"${txtNewPass.hint} and ${txtCnfrmPass.hint}  does not Matched")
            return false
        }
        return true
    }




    interface Handler {

        fun onSubmit(tempPassword: CharSequence,password: CharSequence)
        fun onCancel()
    }


}