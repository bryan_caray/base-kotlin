package com.yondu.canvas.global.mvp

import androidx.appcompat.app.AlertDialog
import com.yondu.canvas.global.api.ApiService
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm

abstract class MvpView {

    interface ActivityFragment {

        val hasBusEvent: Boolean

        val layoutId: Int

        fun onViewCreated()
    }

    interface Presenter {

        val realm: Realm

        val api: ApiService
    }

    interface Dialog {

        val builder: AlertDialog.Builder

        var layoutId: Int

        fun onViewCreated()
    }
}