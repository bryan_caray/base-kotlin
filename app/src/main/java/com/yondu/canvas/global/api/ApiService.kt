package com.yondu.canvas.global.api

import com.yondu.canvas.BuildConfig
import com.yondu.canvas.model.db.Post
import com.yondu.canvas.model.remote.body.UserBody
import com.yondu.canvas.model.remote.response.UserResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Url

interface ApiService{

    @POST("user/register")
    fun postRawRegister(@Body body: UserBody): Observable<UserResponse>

    @POST("user/register/whitelist")
    fun postRawWhitelistRegister(@Body body: UserBody): Observable<UserResponse>



    @POST("user/login")
    fun postRawLogin(@Body body: UserBody): Observable<UserResponse>

    @POST("user/forgot")
    fun postForgot(@Body body: UserBody): Observable<UserResponse>

    @POST("user/change")
    fun postChangePassword(@Body body: UserBody): Observable<UserResponse>

    @GET
    fun posts(@Url url: String ): Observable<List<Post>>

}
