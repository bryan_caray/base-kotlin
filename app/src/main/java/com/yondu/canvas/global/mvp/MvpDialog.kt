package com.yondu.canvas.global.mvp

import android.app.Activity
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.yondu.canvas.R
import com.yondu.canvas.databinding.DialogBinding
import com.yondu.canvas.model.local.CountryDial

abstract class MvpDialog<B : ViewDataBinding>(protected val activity: Activity) : MvpView.Dialog {

    protected val TAG = javaClass.simpleName

    override val builder = AlertDialog.Builder(activity)

    protected lateinit var binding: B

    private lateinit var root: DialogBinding

    private lateinit var dialog: AlertDialog

    open fun initView(): MvpDialog<B> {
        root = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog, null, false)

        root.content.viewStub!!.layoutResource = layoutId
        binding = DataBindingUtil.getBinding(root.content.viewStub!!.inflate())!!

        isStrong()
        setTitle()
        setPositiveButton(null)
        setNegativeButton(null)
        setNeutralButton(null)
        onViewCreated()

        builder.setView(root.root)
        return this
    }

    open fun isStrong(isStrong: Boolean = false) {
        builder.setCancelable(!isStrong)
    }

    open fun setTitle(type: Type = Type.NONE, icon: Drawable? = null, title: CharSequence? = null) {
        root.title.visibility = if (type == Type.NONE && icon == null && title == null) View.GONE else View.VISIBLE
        root.ivTitle.setImageDrawable(if (type != Type.NONE && icon == null) ContextCompat.getDrawable(activity, type.icon) else null)
        root.ivTitle.setColorFilter(ContextCompat.getColor(activity, type.foreground))
        root.tvTitle.setTextColor(ContextCompat.getColor(activity, type.foreground))
        root.tvTitle.typeface = Typeface.DEFAULT_BOLD
        root.tvTitle.text = title ?: type.label
    }

    open fun setPositiveButton(label: CharSequence?) {
        root.bPositive.visibility = if (label != null) View.VISIBLE else View.GONE
        root.bPositive.text = label
    }

    open fun setNegativeButton(label: CharSequence?) {
        root.bNegative.visibility = if (label != null) View.VISIBLE else View.GONE
        root.bNegative.text = label
    }

    open fun setNeutralButton(label: CharSequence?) {
        root.bNeutral.visibility = if (label != null) View.VISIBLE else View.GONE
        root.bNeutral.text = label
    }

    open fun onItemSelected(item: CountryDial) {
        dialog.dismiss()
    }

    open fun onPositiveClicked() {
        dialog.dismiss()
    }

    open fun onNegativeClicked() {
        dialog.dismiss()
    }

    open fun onNeutralClicked() {
        dialog.dismiss()
    }

    open fun create(): AlertDialog {
        dialog = builder.create()

        root.bPositive.setOnClickListener { onPositiveClicked() }
        root.bNegative.setOnClickListener { onNegativeClicked() }
        root.bNeutral.setOnClickListener { onNeutralClicked() }

        dialog.window!!.decorView.setBackgroundColor(Color.RED)
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        return dialog
    }


    enum class Type(val icon: Int, val label: CharSequence, val foreground: Int) {
        NONE(R.drawable.ic_dialog_info, "Information", R.color.black),
        CONFIRM(R.drawable.ic_dialog_confirm, "Question", R.color.blue),
        SUCCESS(R.drawable.ic_dialog_success, "Success", R.color.green),
        ERROR(R.drawable.ic_dialog_error, "Error", R.color.red),
        WARNING(R.drawable.ic_dialog_warning, "Warning", R.color.orange)
    }
}