package com.yondu.canvas.global.util

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import androidx.core.content.ContextCompat
import com.yondu.canvas.R

object ResourceUtil {

    fun setRippleColor(context: Context, drawable: Drawable?, color: Int): Drawable? {
        if (drawable == null) return null
        return RippleDrawable(
            ColorStateList(arrayOf(
                intArrayOf(android.R.attr.state_pressed),
                intArrayOf(android.R.attr.state_enabled),
                intArrayOf(android.R.attr.state_focused, android.R.attr.state_pressed),
                intArrayOf(-android.R.attr.state_enabled),
                intArrayOf()), intArrayOf(
                ContextCompat.getColor(context, color),
                ContextCompat.getColor(context, android.R.color.transparent),
                ContextCompat.getColor(context, color),
                ContextCompat.getColor(context, android.R.color.transparent),
                ContextCompat.getColor(context, android.R.color.transparent))), drawable, null)
    }

    val getDefaultFlagRes = R.drawable.ic_list_country_unknown

    fun getFlagDrawable(context: Context, code: String): Drawable {
        var res = context.resources.getIdentifier("ic_list_country_${code.toLowerCase()}",
            "drawable", context.packageName)
        if (res == 0) res = getDefaultFlagRes

        val bitmap = BitmapFactory.decodeResource(context.resources, res)

        val w = bitmap.width
        val h = bitmap.height
        val scale = 70f

        val matrix = Matrix()
        matrix.postScale(scale / w, scale / h)

        val resized = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true)
        return BitmapDrawable(context.resources, resized)
    }
}