package com.yondu.canvas.global.module.walkthrough

import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.yondu.canvas.databinding.ActivityWalkthroughBinding
import com.yondu.canvas.global.mvp.MvpPagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener




class Walkthrough(binding: ActivityWalkthroughBinding, handler: Handler) {

    private val pager = binding.include.walkPager


    init {

        val adapter = object : MvpPagerAdapter<ConstraintLayout>(listOf(binding.include.page1, binding.include.page2, binding.include.page3)){
            override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
//                container.removeView(obj as View)
            }
        }
        pager.adapter = adapter
        binding.springDotsIndicator.attach(pager)
        binding.include.buttonDone.setOnClickListener{
            handler.onDonePressed()

        }

        pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                var size = pager.getAdapter()!!.getCount()-1
                if(position == size){
                    handler.onLastPage()
                }else if(position in 1..(size - 1)){
                    handler.onOtherPage()
                }else{
                    handler.onFirstPage()
                }

                // Check if this is the page you want.
            }
        })

    }




interface Handler {

    fun onDonePressed()
    fun onLastPage(){}
    fun onFirstPage(){}
    fun onOtherPage(){}
}

}